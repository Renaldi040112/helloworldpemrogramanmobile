import 'package:flutter/material.dart';

class Lat1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        backgroundColor: Colors.blueAccent,
        appBar: AppBar(
          backgroundColor: Colors.red,
          foregroundColor: Colors.brown,
          centerTitle: true,
          title: Text(
            'HALAMAN LATIHAN 1',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
        ),
        body: Center(
          child: Text(
            'KALIMAT PERTAMA',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40,
                color: Colors.black87),
          ),
        ),
      ),
    );
  }
}
