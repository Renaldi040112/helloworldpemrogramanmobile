import 'package:flutter/material.dart';

class LatColRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        backgroundColor: Colors.purpleAccent[100],
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Mencoba Col dan Row'),
          backgroundColor: Colors.deepPurpleAccent,
          foregroundColor: Colors.white,
          shadowColor: Colors.black,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/dualshock.jpg'),
          Text(
            'List Game',
            style:
                TextStyle(fontSize: 24, fontFamily: 'PatrickHand', height: 2.0),
          ),
          Text('Pradita university'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset('assets/images/ps42.jpg'),
              Text(
                "Playstation",
                style: TextStyle(
                    fontSize: 24, fontFamily: 'PatrickHand', height: 2.0),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Dragon Quest",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
              Text(
                "Elder Scrolls V",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
              Text(
                "Disgaea",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
              Text(
                "Kingdom Hearts",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Final Fantasy",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
              Text(
                "Cold Steel",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
              Text(
                "Cold War",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
              Text(
                "Fortnite",
                style: TextStyle(
                    fontSize: 18, fontFamily: 'PatrickHand', height: 2.0),
              ),
            ],
          )
        ]),
      ),
    );
  }
}
