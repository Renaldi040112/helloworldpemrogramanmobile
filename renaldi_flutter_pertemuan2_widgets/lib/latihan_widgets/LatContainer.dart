import 'package:flutter/material.dart';

class LatContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        backgroundColor: Colors.greenAccent,
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Latihan Container'),
          backgroundColor: Colors.grey,
          foregroundColor: Colors.white,
          shadowColor: Colors.black,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/strawberry.png'),
          Text(
            'List Buah-Buah',
            style:
                TextStyle(fontSize: 24, fontFamily: "PatrickHand", height: 2.0),
          ),
          Text('Pradita university'),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 8, color: Colors.indigoAccent),
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Strawberry',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'PatrickHand',
                        height: 2.0,
                        letterSpacing: 20),
                  ),
                ],
              )),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 8, color: Colors.indigoAccent),
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Apple',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'PatrickHand',
                        height: 2.0,
                        letterSpacing: 20),
                  ),
                ],
              )),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 8, color: Colors.indigoAccent),
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Guava',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'PatrickHand',
                        height: 2.0,
                        letterSpacing: 20),
                  ),
                ],
              )),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 8, color: Colors.indigoAccent),
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Mango',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'PatrickHand',
                        height: 2.0,
                        letterSpacing: 20),
                  ),
                ],
              )),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 8, color: Colors.indigoAccent),
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(5)),
              ),
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Melon',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'PatrickHand',
                        height: 2.0,
                        letterSpacing: 20),
                  ),
                ],
              )),
        ]),
      ),
    );
  }
}
