import 'package:flutter/material.dart';

class LatGridViewCount extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Center(
            child: Text(
              'Latihan Gridview',
              style: TextStyle(
                color: Colors.blueAccent,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
          ),
        ),
        body: GridView.count(
          crossAxisCount: 4,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          shrinkWrap: true,
          children: List.generate(
            60,
            (index) {
              return Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 10, color: Colors.black38),
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                    color: Colors.teal),
                child: Text(
                  'Barang\n\n ${index + 1}',
                  textAlign: TextAlign.center,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
