import 'package:flutter/material.dart';

class LatGridViewextent extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Center(
            child: Text(
              'Latihan GridView Extent',
              style: TextStyle(
                color: Colors.blueAccent,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
          ),
        ),
        body: GridView.extent(
          maxCrossAxisExtent: 100,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 20.0,
          shrinkWrap: true,
          children: List.generate(
            60,
            (index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 10, color: Colors.blueAccent),
                  color: Colors.yellow,
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                ),
                child: Text('Barang ${index + 1}'),
              );
            },
          ),
        ),
      ),
    );
  }
}
