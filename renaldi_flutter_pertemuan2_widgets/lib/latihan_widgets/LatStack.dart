import 'package:flutter/material.dart';

class LatStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Latihan Stack'),
        ),
        body: Stack(
          children: [
            Container(
              width: 500,
              height: 500,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: Colors.black,
              ),
              child: const Text(
                'NINTENDO',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
            const CircleAvatar(
              backgroundImage: AssetImage('assets/images/NintendoSwitch.png'),
              radius: 100,
            ),
          ],
        ),
      ),
    );
  }
}
