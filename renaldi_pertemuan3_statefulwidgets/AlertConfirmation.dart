import 'package:flutter/material.dart';

class AlertConfirmation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Alert ...'),
        ),
        body: Center(
          child: MyAppAlertAndDialog(),
        ),
      ),
    );
  }
}

class MyAppAlertAndDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Flutter Basic Alert Demo';
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: MyAlert(),
      ),
    );
  }
}

class MyAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ElevatedButton(
          child: Text('Show alert'),
          onPressed: () async {
            bool result = await showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text('Confirmation'),
                  content: Text('Do you want to go to the next page?'),
                  actions: <Widget>[
                    new ElevatedButton(
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true).pop(
                            false); // dismisses only the dialog and returns false
                      },
                      child: Text('No'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true).pop(
                            false); // dismisses only the dialog and returns false
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SecondRoute()),
                        );
                      },
                      child: Text('Yes'),
                    ),
                  ],
                );
              },
            );
          }),
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyAppAlertAndDialog()),
            );
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}
