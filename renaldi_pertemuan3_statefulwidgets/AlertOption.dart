import 'package:flutter/material.dart';

class AlertOption extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Alert ...'),
        ),
        body: Center(
          child: MyAppAlertAndDialog(),
        ),
      ),
    );
  }
}

class MyAppAlertAndDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Flutter Basic Alert Demo';
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: MyAlert(),
      ),
    );
  }
}

class MyAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ElevatedButton(
          child: Text('Show alert'),
          onPressed: () async {
            Product result = await showOptionDialog(context);
            var dd = result;
          }),
    );
  }

  Future<Product> showOptionDialog(BuildContext context) async {
    var result = await showDialog<Product>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Select Product '),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Product.Apple);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AppleRoute()),
                  );
                },
                child: const Text('Apple'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Product.Samsung);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SamsungRoute()),
                  );
                },
                child: const Text('Samsung'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Product.Oppo);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => OPPORoute()),
                  );
                },
                child: const Text('Oppo'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Product.Redmi);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => redmiRoute()),
                  );
                },
                child: const Text('Redmi'),
              ),
            ],
          );
        });
    return result as Product;
  }
}

class AppleRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("APPLE ROUTE"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}

class SamsungRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SAMSUNG ROUTE"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}

class OPPORoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("OPPPO ROUTE"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}

class redmiRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("redmi ROUTE"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}

enum Product { Apple, Samsung, Oppo, Redmi }
