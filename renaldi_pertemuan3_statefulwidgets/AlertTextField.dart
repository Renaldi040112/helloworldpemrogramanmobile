import 'package:flutter/material.dart';

class AlertTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Alert ...'),
        ),
        body: Center(
          child: MyAppAlertAndDialog(),
        ),
      ),
    );
  }
}

class MyAppAlertAndDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Flutter Basic Alert Demo';
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: MyAlert(),
      ),
    );
  }
}

class MyAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ElevatedButton(
          child: Text('Show alert'),
          onPressed: () async {
            Object result = await showOptionDialog(context);
            var dd = result;
          }),
    );
  }

  Future<Object> showOptionDialog(BuildContext context) async {
    TextEditingController _textFieldController = TextEditingController();
    var result = await showDialog<Object>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Siapa namamu '),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "TextField in Dialog"),
            ),
            actions: <Widget>[
              new ElevatedButton(
                onPressed: () {
                  Navigator.of(context, rootNavigator: true)
                      .pop(_textFieldController);
                },
                child: Text('No'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.of(context, rootNavigator: true)
                      .pop(_textFieldController);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NameRoute()),
                  );
                },
                child: Text('Yes'),
              ),
            ],
          );
        });
    return result as Object;
  }
}

class NameRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Naming Route"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
          child: Text('Go back to first page!'),
        ),
      ),
    );
  }
}
