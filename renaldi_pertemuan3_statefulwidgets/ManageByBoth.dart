import 'package:flutter/material.dart';

class ManageByBoth extends StatelessWidget {
  const ManageByBoth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Manage By Both'),
        ),
        body: const Center(
          child: ParentWidget(),
        ),
      ),
    );
  }
}

class ParentWidget extends StatefulWidget {
  const ParentWidget({Key? key}) : super(key: key);

  @override
  _ParentWidgetState createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> {
  bool _active = false;
  bool _circle = true;

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
    });
  }

  void _handleTapBoxChanged2(bool newValue) {
    setState(() {
      _circle = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: TapboxC(
        circle: _circle,
        active: _active,
        onChanged: _handleTapboxChanged,
        onChanged2: _handleTapBoxChanged2,
      ),
    );
  }
}

//----------------------------- TapboxC ------------------------------

class TapboxC extends StatefulWidget {
  const TapboxC({
    Key? key,
    this.active = false,
    this.circle = true,
    required this.onChanged,
    required this.onChanged2,
  }) : super(key: key);

  final bool active;
  final bool circle;
  final ValueChanged<bool> onChanged;
  final ValueChanged<bool> onChanged2;

  @override
  _TapboxCState createState() => _TapboxCState();
}

class _TapboxCState extends State<TapboxC> {
  bool _highlight = false;
  bool _higlight2 = false;

  void _handleTapDown(TapDownDetails details) {
    setState(() {
      _highlight = true;
      _higlight2 = false;
    });
  }

  void _handleTapUp(TapUpDetails details) {
    setState(() {
      _highlight = false;
      _higlight2 = false;
    });
  }

  void _handleTapCancel() {
    setState(() {
      _highlight = false;
      _higlight2 = false;
    });
  }

  void _handleTap() {
    widget.onChanged(!widget.active);
  }

  void _handleTap2() {
    widget.onChanged2(!widget.circle);
    _higlight2 = true;
  }

  @override
  Widget build(BuildContext context) {
    // This example adds a green border on tap down.
    // On tap up, the square changes to the opposite state.
    return GestureDetector(
      onTapDown: _handleTapDown, // Handle the tap events in the order that
      onTapUp: _handleTapUp, // they occur: down, up, tap, cancel
      onTap: _handleTap,
      onTapCancel: _handleTapCancel,
      onDoubleTap: _handleTap2,
      child: Container(
        child: Center(
          child: Text(widget.active ? 'Active' : 'Inactive',
              style: const TextStyle(fontSize: 32.0, color: Colors.white)),
        ),
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
            color: widget.active ? Colors.lightGreen[700] : Colors.grey[600],
            border: _highlight
                ? Border.all(
                    color: Colors.teal[700]!,
                    width: 10.0,
                  )
                : null,
            borderRadius: widget.circle
                ? BorderRadius.circular(100)
                : BorderRadius.circular(0),
            boxShadow: _higlight2
                ? [
                    BoxShadow(
                      color: Colors.orange.withOpacity(0.9),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3),
                    )
                  ]
                : null),
      ),
    );
  }
}
