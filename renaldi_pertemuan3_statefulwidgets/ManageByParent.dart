import 'package:flutter/material.dart';

class ManageByParent extends StatelessWidget {
  const ManageByParent({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Manage By Parent'),
        ),
        body: const Center(
          child: ParentWidget(),
        ),
      ),
    );
  }
}

class ParentWidget extends StatefulWidget {
  const ParentWidget({Key? key}) : super(key: key);
  @override
  _ParentWidgetState createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> {
  bool _active = false;
  bool _circle = true;

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
    });
  }

  void _handleTapboxChanged2(bool newValue) {
    setState(() {
      _circle = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: TapboxB(
        active: _active,
        onChanged: _handleTapboxChanged,
        circleActive: _circle,
        onChanged2: _handleTapboxChanged2,
      ),
    );
  }
}

class TapboxB extends StatelessWidget {
  const TapboxB({
    Key? key,
    this.active = false,
    this.circleActive = false,
    required this.onChanged,
    required this.onChanged2,
  }) : super(key: key);

  final bool active;
  final bool circleActive;
  final ValueChanged<bool> onChanged;
  final ValueChanged<bool> onChanged2;

  void _handleTap() {
    onChanged(!active);
  }

  void _handleTap2() {
    onChanged2(!circleActive);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      onDoubleTap: _handleTap2,
      child: Container(
        child: Center(
          child: Text(
            active ? 'Active' : 'Inactive',
            style: const TextStyle(fontSize: 32.0, color: Colors.white),
          ),
        ),
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: active ? Colors.lightGreen[700] : Colors.grey[600],
          borderRadius: circleActive
              ? BorderRadius.circular(100)
              : BorderRadius.circular(0),
        ),
      ),
    );
  }
}
