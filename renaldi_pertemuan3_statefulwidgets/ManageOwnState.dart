import 'package:flutter/material.dart';

class ManageOwnState extends StatelessWidget {
  const ManageOwnState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Manage Own State'),
        ),
        body: const Center(
          child: TapboxA(),
        ),
      ),
    );
  }
}

class TapboxA extends StatefulWidget {
  const TapboxA({Key? key}) : super(key: key);

  @override
  _TapboxAState createState() => _TapboxAState();
}

class _TapboxAState extends State<TapboxA> {
  bool _active = false;
  bool _circle = true;

  void _handleTap() {
    setState(() {
      _active = !_active;
    });
  }

  void _handleDoubleTap() {
    setState(() {
      _circle = !_circle;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _handleTap,
      onDoubleTap: _handleDoubleTap,
      child: Container(
        child: Center(
          child: Text(
            _active ? 'Active' : 'Inactive',
            style: const TextStyle(fontSize: 32.0, color: Colors.white),
          ),
        ),
        width: 200.0,
        height: 200.0,
        decoration: BoxDecoration(
          color: _active ? Colors.lightGreen[700] : Colors.grey[600],
          borderRadius:
              _circle ? BorderRadius.circular(100) : BorderRadius.circular(0),
        ),
      ),
    );
  }
}

//------------------------- MyApp ----------------------------------
