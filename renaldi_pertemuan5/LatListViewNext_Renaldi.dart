import 'package:flutter/material.dart';
import 'package:flutter_application_1/IoT/Mahasiswa.dart';
import 'package:flutter_application_1/IoT/MahasiswaClass.dart';

class LatListViewNext_Renaldi extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.blueGrey[900],
            title: Center(
              child: Text(
                'Flutter GridView Demo',
              ),
            ),
          ),
          body: MyListMahasiswa()),
    );
  }
}

class MyList1 extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        Container(
          height: 50,
          color: Colors.amber[600],
          child: const Center(child: Text('Entry A')),
        ),
        Container(
          height: 50,
          color: Colors.amber[500],
          child: const Center(child: Text('Entry B')),
        ),
        Container(
          height: 50,
          color: Colors.amber[100],
          child: const Center(child: Text('Entry C')),
        ),
      ],
    );
  }
}

class MyList2 extends StatelessWidget {
  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: entries.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 50,
            color: Colors.amber[colorCodes[index]],
            child: Center(child: Text('Entry ${entries[index]}')),
          );
        });
  }
}

class MyList3 extends StatelessWidget {
  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 400];

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: entries.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 50,
          color: Colors.amber[colorCodes[index]],
          child: Center(child: Text('Entry ${entries[index]}')),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}

class MyListMahasiswa extends StatelessWidget {
  final List<int> colorCodes = <int>[
    300,
    200,
    100,
    100,
    200,
    300,
    300,
    200,
    100,
    100,
    200,
    300
  ];
  final List<MahasiswaClass> mahasiswas = [
    MahasiswaClass(10000, 'Dodit', 23),
    MahasiswaClass(10001, 'Melisha', 22),
    MahasiswaClass(10002, 'Aniza', 20),
    MahasiswaClass(10003, 'Kevin', 23),
    MahasiswaClass(10004, 'Daniel', 19),
    MahasiswaClass(10005, 'Caela', 22),
    MahasiswaClass(10006, 'Alexia', 22),
    MahasiswaClass(10007, 'Yuki', 19),
    MahasiswaClass(10008, 'Jagoki', 20),
    MahasiswaClass(10009, 'Layla', 24),
    MahasiswaClass(10010, 'Juki ', 22),
  ];

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(25),
      itemCount: mahasiswas.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 100,
          color: Colors.blue[colorCodes[index]],
          child: Center(child: Text('${mahasiswas[index]}')),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}
