class MahasiswaClass {
  int Id;
  String Nama;
  int age;

  MahasiswaClass(this.Id, this.Nama, this.age);

  @override
  String toString() {
    return 'Id: ${this.Id}\n\nNama: ${this.Nama}\n\nUmur: ${this.age}';
  }
}
