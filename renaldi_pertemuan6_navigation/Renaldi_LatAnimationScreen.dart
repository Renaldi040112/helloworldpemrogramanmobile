import 'package:flutter/material.dart';

class Renaldi_LatAnimationScreen extends StatelessWidget {
  const Renaldi_LatAnimationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Transition Demo',
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Main Screen'),
      ),
      body: Column(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const DetailScreen();
              }));
            },
            child: Hero(
              tag: 'imageHero',
              child: Image.network(
                'https://pbs.twimg.com/media/E9uj1g3XIAk_u6P.png',
                width: 300,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const DetailScreen2();
              }));
            },
            child: Hero(
              tag: 'imageHero2',
              child: Image.network(
                'https://i.pinimg.com/originals/e4/2a/06/e42a066af2f1cdc6737e3a03e7334875.png',
                width: 300,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Padding(
          padding: EdgeInsets.all(150),
          child: Hero(
            tag: 'imageHero',
            child: Image.network(
              'https://pbs.twimg.com/media/E9uj0NDX0AAPE9g.png',
            ),
          ),
        ),
      ),
    );
  }
}

class DetailScreen2 extends StatelessWidget {
  const DetailScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Padding(
          padding: EdgeInsets.all(150),
          child: Hero(
            tag: 'imageHero2',
            child: Image.network(
              'https://libredd.it/img/zhe7ovfo2lq71.png',
            ),
          ),
        ),
      ),
    );
  }
}
