import 'package:flutter/material.dart';

class Renaldi_LatPassListDetail extends StatelessWidget {
  const Renaldi_LatPassListDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Passing Data',
      home: TodosScreen(
        todos: List.generate(
          20,
          (i) => Todo('Kalkulasi $i', 'Ini adalah perhitungan $i', i + 2, i),
        ),
      ),
    );
  }
}

class Todo {
  final String title;
  final String description;
  final int A;
  final int B;

  const Todo(this.title, this.description, this.A, this.B);
}

class TodosScreen extends StatelessWidget {
  const TodosScreen({Key? key, required this.todos}) : super(key: key);
  final List<Todo> todos;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todos'),
      ),
      body: ListView.builder(
        itemCount: todos.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(todos[index].title),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailScreen(todo: todos[index]),
                ),
              );
            },
          );
        },
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key, required this.todo}) : super(key: key);
  final Todo todo;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(todo.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(),
          Text(todo.description + '\n'),
          Text(
            todo.A.toString() +
                'x' +
                todo.B.toString() +
                '=' +
                (todo.A * todo.B).toString(),
            textAlign: TextAlign.left,
          )
        ],
      ),
    );
  }
}
