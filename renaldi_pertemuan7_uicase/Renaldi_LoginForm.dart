import 'package:flutter/material.dart';

import 'User.dart';
import 'UserDetail.dart';

class Renaldi_LoginForm extends StatefulWidget {
  static const routeName = '/LoginForm';
  @override
  _LoginFormFormState createState() => _LoginFormFormState();
}

class _LoginFormFormState extends State<Renaldi_LoginForm> {
  final _formKey = GlobalKey<FormState>();

  bool _obscureText = true;
  User user = new User("User Id", "Password");

  void togglePassword() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  /*
  Future<void> submit() async {
    if (user.userId == "Andi" && user.password == "123") {
      bool result = await showAlertDialog(context, "Success");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => UserDetail(user: user),
        ),
      );
    } else {
      showAlertDialog(context, "Incorrect");
    }
  }*/

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double passwordWidth = width * 0.65;
    double buttonShowHideWidth = width * 0.20;

    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onChanged: (value) {
                      user.userId = value;
                      setState(() {
                        user = user;
                      });
                    },
                    decoration: new InputDecoration(
                      hintText: "User ID",
                      labelText: user.userId,
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'User ID tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                          width: passwordWidth,
                          child: TextFormField(
                            obscureText: _obscureText,
                            onChanged: (value) {
                              user.password = value;
                              setState(() {
                                user = user;
                              });
                            },
                            decoration: new InputDecoration(
                              hintText: "Password",
                              labelText: user.password,
                              icon: Icon(Icons.lock),
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Password tidak boleh kosong';
                              }
                              return null;
                            },
                          )),
                      Container(
                        margin: const EdgeInsets.all(12.0),
                        width: buttonShowHideWidth,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                fixedSize: Size(20, 20),
                                primary: _obscureText
                                    ? Colors.blue
                                    : Colors.deepOrange),
                            onPressed: togglePassword,
                            child: Text(_obscureText ? "Show" : "Hide")),
                      )
                    ],
                  ),
                ),
                /*
                ElevatedButton(
                  child: Text(
                    "Login",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      submit();
                    }
                  },
                ),*/
                Padding(
                  padding: const EdgeInsets.all(80),
                ),
                Table(
                  border: TableBorder.all(),
                  columnWidths: const <int, TableColumnWidth>{
                    0: FixedColumnWidth(50),
                    1: FixedColumnWidth(150),
                    2: FlexColumnWidth(),
                  },
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  children: [
                    TableRow(
                      children: [
                        Container(
                          height: 64,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          color: Colors.white,
                          child: Text("1",
                              style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              )),
                        ),
                        Container(
                          height: 64,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.centerLeft,
                          color: Colors.green,
                          child: Text("User ID",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              )),
                        ),
                        Container(
                          height: 64,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.centerLeft,
                          color: Colors.green,
                          child: Text(user.userId,
                              style: TextStyle(
                                color: Colors.yellow,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              )),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        Container(
                          height: 64,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          color: Colors.white,
                          child: Text("2",
                              style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              )),
                        ),
                        Container(
                          height: 64,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.centerLeft,
                          color: Colors.green,
                          child: Text("Password",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              )),
                        ),
                        Container(
                          height: 64,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.centerLeft,
                          color: Colors.green,
                          child: Text(user.password,
                              style: TextStyle(
                                color: Colors.yellow,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              )),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Future<bool> showAlertDialog(BuildContext context, String info) async {
  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.pop(context, true);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Login Info"),
    content: Text(info),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  var result = await showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
  return result;
}
